package pl.sdaacademy.aoeapi.details_loader.repository.api;

public class CivilizationBonus {
    private String bonus;

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
