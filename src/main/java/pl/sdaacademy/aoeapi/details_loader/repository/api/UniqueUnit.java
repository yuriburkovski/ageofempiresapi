package pl.sdaacademy.aoeapi.details_loader.repository.api;

public class UniqueUnit {
    private String unit;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
